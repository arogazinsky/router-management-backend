import re

import fabric
from fabric import Result

from core.models import SSHConnection, Router, CommandToExecute, Script, ScriptImplementation


def set_connection(ssh_connection: SSHConnection):
    return fabric.Connection(
        host=ssh_connection.host_name,
        user=ssh_connection.username,
        connect_timeout=3,
        connect_kwargs={
            "password": ssh_connection.password,
            "allow_agent": False,
            "look_for_keys": False
        }
    )


def get_cpu_usage(launch_command):
    result: Result = launch_command("system resource cpu print")
    match = re.search(r"(\S+)%\s+\S+%\s+\S+%", result.stdout)
    if match is None:
        return None
    return match.group(1)


def get_memory_info(launch_command):
    result: Result = launch_command("system resource print")
    match = re.search(r"free-memory:\s+(\S+)", result.stdout)
    if match is None:
        return None
    return match.group(1)


interface_status = {
    "D": "dynamic",
    "X": "disabled",
    "R": "running",
    "S": "slave"
}


def get_interfaces_info(launch_command):
    result: Result = launch_command("interface print")
    interfaces = re.findall("\d+\s+([DXRS])\s+(\w+)\s+\w+\s+\d+\s*", result.stdout)
    return [
        {
            "name": item[1],
            "status": interface_status.get(item[0], item[0])
        } for item in interfaces
    ]


class SSHProvider:
    def __init__(self, router: Router):
        self.router = router
        self.connection = set_connection(router.ssh_connection)

    def launch_command(self, command):
        try:
            return self.connection.run(command, hide=True, warn=True)
        except Exception as exc:
            return Result(
                stdout=str(exc),
                command=command,
                exited=-1,
                connection=self.connection
            )

    def run_action(self, action_name):
        implementation = ScriptImplementation.objects.get(router=self.router, script__name=action_name)
        commands = implementation.commandtoexecute_set.all().order_by("order").values_list("value", flat=True)
        results = [self.launch_command(command) for command in commands]
        return [(result.command, result.stdout, result.exited) for result in results]

    def get_device_information(self):
        runner = self.launch_command
        return {
            "interfaces": get_interfaces_info(runner),
            "cpu_usage": get_cpu_usage(runner),
            "free_memory": get_memory_info(runner)
        }
