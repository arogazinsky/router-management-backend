import re

import invoke

from controller.providers.ssh import SSHProvider
from core.models import Router


PING_PACKETS_NUMBER = 1
PING_PACKET_TIMEOUT = 1


providers_cache = {}


def ping(host, packets_number=PING_PACKETS_NUMBER):
    result = invoke.run(f"ping -c {packets_number} -w {packets_number * PING_PACKET_TIMEOUT} -q {host}", warn=True, hide=True)
    return result.stdout, result.exited


def get_latency(host):
    command_output, exitstatus = ping(host)
    if exitstatus != 0:
        return None
    re_result = "rtt min\/avg\/max\/mdev = \S+\/(\S+)\/\S+\/\S+ ms"
    match = re.search(re_result, str(command_output))
    if match is None:
        return None
    return match.group(1)


def check_router_alive(router):
    host = router.ssh_connection.host_name
    exitstatus = ping(host, 1)[1]
    return exitstatus == 0


def get_provider_for_router(router: Router):
    if router.name not in providers_cache:
        providers_cache[router.name] = SSHProvider(router)
    return providers_cache[router.name]


def get_device_info(router):
    provider = get_provider_for_router(router)
    return {
        "latency": get_latency(router.ssh_connection.host_name),
        **provider.get_device_information()
    }


def run_action(router, action_name):
    provider = get_provider_for_router(router)
    return provider.run_action(action_name)
