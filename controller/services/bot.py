import logging
from signal import SIGINT, SIGTERM, SIGABRT, signal
from enum import Enum
from functools import partial
from time import sleep

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, KeyboardButton, \
    ReplyKeyboardRemove, ParseMode, Update, Bot
from telegram.ext import Updater, CommandHandler, ConversationHandler, CallbackQueryHandler, MessageHandler, \
    RegexHandler
from telegram.ext.filters import Filters

from core.models import Router, User, Settings, Script, ScriptUser, Session
import math

import controller
from controller.services.permissions import has_permission_to_launch_action

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

AUTH, GET_TOKEN, DEVICES_LIST, INFO, ACTIONS, ACTION_DETAILS = range(6)
PAGE_LEN = 3


CHECK_CHAR = "✅"
ERROR_CHAR = "❌"


class SettingsEnum(Enum):
    TELEGRAM_TOKEN = "TelegramBotToken"
    AUTH_TOKEN = "AuthToken"
    COMMON_CHANNEL_ID = "CommonChannelId"
    BOT_STATE = "BotState"
    ML_MODE = "MLMode"


def get_parameter(enumitem):
    try:
        return Settings.objects.get(name=enumitem.value).value
    except Settings.DoesNotExist:
        return None


def get_username(update: Update):
    return update.effective_user.username


def get_session(session_key):
    return Session.objects.get_or_create(session_id=session_key)[0].payload


def set_session(session_key, value):
    session = Session.objects.get_or_create(session_id=session_key)[0]
    session.payload = value
    session.save()


def authenticate(bot: Bot, update: Update):
    username = get_username(update)
    try:
        User.objects.get(username=username)
    except User.DoesNotExist:
        update.message.reply_text("Вас нет в списке разрешенных пользователей. Введите токен авторизации")
        return GET_TOKEN
    return list_devices(bot, update)


def get_token_from_user(bot: Bot, update: Update):
    auth_token = get_parameter(SettingsEnum.AUTH_TOKEN)
    if auth_token == update.message.text:
        user = User.objects.create(username=get_username(update))
        for script in Script.objects.all():
            ScriptUser.objects.create(
                script=script,
                user=user
            )
        update.message.reply_text("Вы добавлены в список разрешенных пользователей")
        return list_devices(bot, update)
    else:
        update.message.reply_text("Неверный токен. Поробуйте ещё раз.")
        return GET_TOKEN


def get_routers_list_keyboard():
    routers = Router.objects.all()
    options = []
    for router in routers:
        is_alive = controller.check_router_alive(router)
        state = CHECK_CHAR if is_alive else ERROR_CHAR
        options.append((router.name, state))
    keyboard = [[InlineKeyboardButton(f"{router_name} {state}", callback_data=router_name)]
                for router_name, state in options]
    keyboard.append([InlineKeyboardButton(f"Обновить", callback_data="обновить")])
    keyboard_markup = InlineKeyboardMarkup(keyboard)
    return keyboard_markup


def list_devices(bot, update: Update):
    reply_markup = ReplyKeyboardRemove()
    message = update.message.reply_text("Получение списка устройств ...", reply_markup=reply_markup)
    message.reply_text("Выберите роутер:", reply_markup=get_routers_list_keyboard())
    return DEVICES_LIST


def format_interfaces(interfaces):
    formatted_interfaces = [f"Название - Статус"]
    for item in interfaces:
        formatted_interfaces.append(f"{item['name']} - {item['status']}")
    return "\n".join(formatted_interfaces)


def device_info(bot, update: Update):
    update.callback_query.answer()

    selected_router = update.callback_query.data
    if selected_router == "обновить":
        update.callback_query.edit_message_reply_markup(reply_markup=get_routers_list_keyboard())
        return DEVICES_LIST

    update.callback_query.message.reply_text("Запрос данных ...")
    router = Router.objects.get(name=selected_router)
    device_info = controller.get_device_info(router)
    availability = device_info.get("latency") or "N/A"
    cpu_usage = device_info.get("cpu_usage") or "N/A"
    free_memory = device_info.get("free_memory") or "N/A"
    interfaces = device_info.get("interfaces")
    interfaces = format_interfaces(interfaces) if interfaces else "N/A"

    message = "\n".join([
        f"Роутер _{router.name}_.",
        f"Доступность через интернет: _{availability} ms_;",
        "Состояние портов:",
        f"`{interfaces}`",
        f"Нагрузка CPU: _{cpu_usage}%_;",
        f"Доступный объём ОЗУ: _{free_memory}_."
    ])
    reply_markup = ReplyKeyboardMarkup([["Действия", "Назад"]], resize_keyboard=True)
    update.callback_query.message.reply_markdown(message, reply_markup=reply_markup)
    update.callback_query.message.delete()
    username = update.callback_query.from_user.username
    current_user_state = get_session(username)
    current_user_state.update(last_selected_router_name=selected_router)
    set_session(username, current_user_state)
    return INFO


def get_actions_page_keyboard(router_name, session):
    page_number = session.get("page_number", 0)
    scripts = Script.objects.filter(scriptimplementation__router__name=router_name).order_by("id")
    action_names = scripts.values_list("name", flat=True)
    if page_number + 1 > math.ceil(action_names.count() / PAGE_LEN):
        page_number = 0
        session["page_number"] = 0
    current_page_actions = list(action_names[page_number * PAGE_LEN: (page_number + 1) * PAGE_LEN])
    nav_buttons = ["В начало"]
    if page_number > 0:
        nav_buttons.append("<<")
    if page_number < math.ceil(scripts.count() / PAGE_LEN) - 1:
        nav_buttons.append(">>")
    keyboard = [[KeyboardButton(action)] for action in current_page_actions]
    keyboard.append([KeyboardButton(nav) for nav in nav_buttons])
    return keyboard


def device_actions(bot, update: Update):
    username = update.effective_user.username
    session = get_session(username)
    router_name = session.get("last_selected_router_name", "N/A")
    keyboard = get_actions_page_keyboard(router_name, session)
    update.message.reply_markdown(f"Выберите действие для роутера _{router_name}_", reply_markup=ReplyKeyboardMarkup(keyboard))
    set_session(update.effective_user.username, session)
    return ACTIONS


actions_mapping = {
    "Выключить NAT": "DisableNAT",
    "Выключить/включить порт": "SwitchPort",
    "Перезагрузить маршрутизатор": "RebootRouter"
}


def navigate_actions(bot, update: Update):
    username = update.effective_user.username
    session = get_session(username)
    page_number = session.get("page_number", 0)
    if update.message.text == ">>":
        page_number += 1
    elif update.message.text == "<<":
        page_number -= 1
    session["page_number"] = page_number
    set_session(username, session)
    return device_actions(bot, update)


def action_details(bot, update: Update):
    username = update.message.from_user.username
    action_name = update.message.text
    reply_markup = ReplyKeyboardMarkup([["Действия", "В начало"]], resize_keyboard=True)
    reply = partial(update.message.reply_markdown, reply_markup=reply_markup)

    if not has_permission_to_launch_action(username, action_name):
        reply(f"У вас нет прав для выполнения этого действия")
        return ACTION_DETAILS

    router_name = get_session(username).get("last_selected_router_name", "N/A")
    router = Router.objects.get(name=router_name)
    results = controller.run_action(router, action_name)
    for command, result, exit_code in results:
        message = "\n".join([
            f"Роутер _{router_name}_.",
            "Выполнена команда:",
            f"`{command}`",
            "Результат выполнения команды:",
            f"`{result}`",
            "Код завершения:",
            f"`{exit_code}`"
        ])
        reply(message)
        send_history_message(bot, username, router_name, command, result, exit_code)
    return ACTION_DETAILS


def send_history_message(bot, username, router_name, code, result, exit_code):
    message = "\n".join([
        f"Пользователь _{username}_ выполнил команду:",
        f"`{code}`",
        f"на роутере _{router_name}_.",
        "Результат выполнения команды:",
        f"`{result}`",
        "Код завершения:",
        f"`{exit_code}`"
    ])
    chat_id = get_parameter(SettingsEnum.COMMON_CHANNEL_ID)
    bot.send_message(chat_id=chat_id, text=message, parse_mode=ParseMode.MARKDOWN)


def help(bot, update):
    message = "Данный бот предназначен для оперативного управления сетевой инфраструктурой. Нажмите <i>Cтарт</i>, чтобы начать взаимодействие с ботом."
    update.message.reply_html(message, reply_markup=ReplyKeyboardMarkup([["Старт"]], resize_keyboard=True))


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


active_bot_setting_name = "BotState"


def get_active_state():
    try:
        value = Settings.objects.get(name=SettingsEnum.BOT_STATE.value).value
        return value.lower() == "active"
    except Settings.DoesNotExist:
        return False


def database_checker_loop(updater: Updater, delay_interval=1):
    while True:
        new_active = get_active_state()
        if not new_active:
            updater.stop()
            logger.warning('Bot stopped')
            return
        sleep(delay_interval)


def launch_bot():
    has_all_credentials = all(get_parameter(item) is not None
                              for item in [SettingsEnum.TELEGRAM_TOKEN,
                                           SettingsEnum.AUTH_TOKEN,
                                           SettingsEnum.COMMON_CHANNEL_ID]) and get_active_state()
    if not has_all_credentials:
        logger.info("Required bot credentials were not provided")
        return

    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler('start', authenticate),
            CommandHandler('help', help)
        ],
        states={
            GET_TOKEN: [
                MessageHandler(Filters.text, get_token_from_user),
            ],
            DEVICES_LIST: [
                CallbackQueryHandler(device_info)
            ],
            INFO: [
                RegexHandler('^Действия$', device_actions),
                RegexHandler('^Назад$', list_devices),
            ],
            ACTIONS: [
                RegexHandler('^В начало$', list_devices),
                RegexHandler('^<<$', navigate_actions),
                RegexHandler('^>>$', navigate_actions),
                MessageHandler(Filters.text, action_details),
            ],
            ACTION_DETAILS: [
                RegexHandler('^Действия$', device_actions),
                RegexHandler('^В начало$', list_devices),
            ]
        },
        fallbacks=[
            CommandHandler('help', help),
        ],
        allow_reentry=True
    )
    # Create the Updater and pass it your bot's token.
    telegram_token = get_parameter(SettingsEnum.TELEGRAM_TOKEN)
    updater = Updater(telegram_token)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(conv_handler)
    dispatcher.add_error_handler(error)

    # Stop the bot if the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    for sig in (SIGINT, SIGTERM, SIGABRT):
        signal(sig, updater.signal_handler)
    updater.start_polling()
    logger.warning("Bot started")
    database_checker_loop(updater, 5)


def bot_loop():
    while True:
        try:
            launch_bot()
        except Exception as e:
            logger.warning(e)
        logger.info("Bot cycle finished")
        sleep(5)


if __name__ == '__main__':
    launch_bot()
