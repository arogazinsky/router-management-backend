import yaml
from os import path

from django.db.transaction import atomic

from core.models import SSHConnection, Router, CommandToExecute, Script, User, ScriptUser, Settings, \
    ScriptImplementation


def create_routers(source_routers):
    for router_data in source_routers:
        ssh_connection_info = router_data["SshConnectionInfo"]
        connection = SSHConnection.objects.create(
            host_name=ssh_connection_info["host"],
            port=ssh_connection_info["port"],
            username=ssh_connection_info["username"],
            password=ssh_connection_info["password"],
            # TODO: public_ssh_key
        )
        Router.objects.create(
            name=router_data["RouterName"],
            ssh_connection=connection
        )


def create_scripts(source_scripts):
    for script_data in source_scripts:
        script = Script.objects.create(
            name=script_data["ScriptName"],
            description_text=script_data["Description"]
        )
        for username in script_data["AllowForUsers"]:
            user = User.objects.get(username=username)
            ScriptUser.objects.create(
                script=script,
                user=user
            )
        for router_data in script_data["Implementations"]:
            router = Router.objects.get_or_create(name=router_data["RouterName"])[0]
            implementation = ScriptImplementation.objects.create(script=script, router=router)
            for idx, command_text in enumerate(router_data["CommandsToExecute"]):
                CommandToExecute.objects.create(
                    value=command_text,
                    order=idx,
                    implementation=implementation
                )


def create_settings(source_data):
    add_settings_parameter = Settings.objects.create
    for key, value in source_data.items():
        if isinstance(value, (int, str, bool)):
            add_settings_parameter(
                name=key,
                value=value
            )


def create_users(user_list):
    for source_username in user_list:
        User.objects.create(
            username=source_username
        )


def truncate_db(models):
    for model in models:
        model.delete().execute()


def load_data_from_file(filepath):
    stream = open(path.abspath(filepath))
    data = yaml.load(stream)
    with atomic():
        # schema.recreate_schema()
        create_users(data.pop("AllowUsers", []))
        create_routers(data.pop("Routers", []))
        create_scripts(data.pop("Scripts", []))
        create_settings(data)


if __name__ == '__main__':
    load_data_from_file("config.yaml")
