from core.models import ScriptUser


def has_permission_to_launch_action(username, action_name):
    queryset = ScriptUser.objects.filter(user__username=username, script__name=action_name, is_allowed=True)
    return queryset.exists()
