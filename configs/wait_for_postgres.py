import socket
import time

import os

port = int(os.getenv("DATABASE_PORT"))

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((os.getenv("DATABASE_HOST"), port))
        s.close()
        break
    except socket.error as ex:
        time.sleep(0.1)
