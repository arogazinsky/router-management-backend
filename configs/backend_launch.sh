#!/usr/bin/env bash
python ./configs/wait_for_postgres.py
python manage.py migrate
# python manage.py loadfromconfig ./configs/bot_config.yaml
uwsgi --ini ./configs/uwsgi.ini