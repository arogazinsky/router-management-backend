"""router URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_extensions.routers import ExtendedSimpleRouter
from rest_framework.authtoken.views import obtain_auth_token

from core import views
from core.views import SettingsView, process_csv_endpoint, get_outliers_view

router = ExtendedSimpleRouter()
router.register('users', views.TelegramUserView)
router.register('routers', views.RouterView)
router.register('scripts', views.ScriptView).register('implementations', views.ImplementationView,
                                                      base_name="script-implementations",
                                                      parents_query_lookups=["script_id"])
router.register('implementations', views.ImplementationsGroupView)


urlpatterns = [
    path('__router_admin__/', admin.site.urls),
    path("api/", include(router.urls)),
    # path("process_pcap", pcap_endpoint),
    path("process_csv", process_csv_endpoint),
    path("get_outliers", get_outliers_view),
    path("api/settings", SettingsView.as_view()),
    path("login/", obtain_auth_token)
]
