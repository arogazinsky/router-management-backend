import time
from io import BytesIO
from os import remove
from os.path import join

import pandas
from django.http import FileResponse
from rest_framework import status
from rest_framework.decorators import action, api_view
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet, ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from django.core.cache import cache

from controller.services.bot import get_parameter, SettingsEnum
from core.ml import fit_predict, add_new_data_from_csv, get_outliers
from core.models import Router, Script, User, ScriptImplementation, ImplementationsGroup, TrafficAnalysisResult
from core.rest_utils import DEFAULT_MODE, convert_pcap, dataframe_to_file, save_attack_records_in_database
from core.serializers import RouterSerializer, ScriptSerializer, TelegramUserSerializer, ScriptImplementationSerializer, \
    PasswordResetSerializer, SettingsSerializer, ImplementationsGroupSerializer, TrafficAnalysisResultSerializer
from router.settings import PCAP_DIR, CSV_DIR


all_without_patch = [m for m in ViewSet.http_method_names if m != 'patch']


class TelegramUserView(ModelViewSet):
    serializer_class = TelegramUserSerializer
    queryset = User.objects.all()
    http_method_names = all_without_patch
    # permission_classes = [IsAdminUser]

    @action(detail=True, methods=['post'])
    def reset_password(self, request, pk=None):
        user = self.get_object()
        serializer = PasswordResetSerializer(data=request.data, context={"user": user})
        if serializer.is_valid():
            serializer.save()
            return Response({'status': 'new password set'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class RouterView(ModelViewSet):
    serializer_class = RouterSerializer
    queryset = Router.objects.all()
    http_method_names = all_without_patch
    # permission_classes = [IsAdminUser]


class ImplementationView(NestedViewSetMixin, ModelViewSet):
    serializer_class = ScriptImplementationSerializer
    queryset = ScriptImplementation.objects.all()
    http_method_names = all_without_patch
    # permission_classes = [IsAdminUser]

    def get_serializer_context(self):
        context = super().get_serializer_context()
        parent_kwargs = self.get_parents_query_dict()
        return {
            "script_id": parent_kwargs["script_id"],
            **context
        }


class ScriptView(ModelViewSet):
    serializer_class = ScriptSerializer
    queryset = Script.objects.all()
    http_method_names = all_without_patch
    # permission_classes = [IsAdminUser]


class SettingsView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        response = SettingsSerializer.get_json()
        return Response(response)

    def post(self, request):
        serializer = SettingsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"detail": "success"})


class ImplementationsGroupView(ModelViewSet):
    serializer_class = ImplementationsGroupSerializer
    queryset = ImplementationsGroup.objects.all()
    http_method_names = all_without_patch


# @api_view(["POST"])
# def pcap_endpoint(request, **kwargs):
#     file_storage = request.FILES.get("pcap")
#     if file_storage is None:
#         return Response("File is not presented", status=400)
#     with open(join(PCAP_DIR, file_storage.name), "wb") as f:
#         f.write(file_storage.read())
#     convert_pcap(PCAP_DIR, CSV_DIR)
#     csv_path = join(CSV_DIR, f"{file_storage.name}_Flow.csv")
#     add_new_data_from_csv(csv_path)
#     time.sleep(1)
#     remove(join(PCAP_DIR, file_storage.name))
#     remove(csv_path)
#     return Response("OK")


@api_view(["POST"])
def process_csv_endpoint(request, **kwargs):
    input_file = request.FILES.get("csv")
    if input_file is None:
        return Response("File is not presented", status=400)
    else:
        add_new_data_from_csv(input_file)
    return Response("OK")


@api_view(["GET"])
def get_outliers_view(request, **kwargs):
    buffer = BytesIO()
    buffer.write(cache.get("outliers.gzip"))
    buffer.seek(0)
    df = pandas.read_pickle(buffer, compression="gzip")
    data = df.to_dict("records")
    return Response(data=data)
