from io import BytesIO, StringIO
from os import chdir, getcwd
from os.path import join

from invoke import run, Result

from core.ml import ATTACK_LABEL
from core.models import TrafficAnalysisResult
from core.serializers import TrafficAnalysisResultSerializer
from router.settings import CICFLOWMETER_PATH, CICFLOWMETER_BIN

DEFAULT_MODE = 'predict'


def convert_pcap(pcap_dir, output_dir):
    current_path = getcwd()
    chdir(CICFLOWMETER_PATH)
    cmd = f'{CICFLOWMETER_BIN} "{pcap_dir}" "{output_dir}"'
    result: Result = run(cmd)
    chdir(current_path)
    if result.return_code != 0:
        raise RuntimeError("Cicflowmeter returned nonzero status")


def dataframe_to_file(dataframe, method_name, is_binary=False):
    if is_binary:
        buf = BytesIO()
    else:
        buf = StringIO()
    method = getattr(dataframe, method_name, lambda *args: None)
    method(buf)
    buf.seek(0)
    return buf


def save_attack_records_in_database(analysis_result_df):
    analysis_result_df = analysis_result_df[analysis_result_df.predicted_label == ATTACK_LABEL]
    records = analysis_result_df.to_dict('records')
    serializer = TrafficAnalysisResultSerializer(data=records, many=True)
    serializer.is_valid(raise_exception=True)
    pre_objects = [TrafficAnalysisResult(**record) for record in serializer.validated_data]
    TrafficAnalysisResult.objects.bulk_create(pre_objects)
