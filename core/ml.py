import math
from collections import OrderedDict
from threading import Thread

from combo.models.detector_comb import SimpleDetectorAggregator
from pyod.models.iforest import IForest
from pyod.models.lof import LOF
from pyod.models.mcd import MCD
from pyod.models.pca import PCA
from sklearn import preprocessing

from sqlalchemy import create_engine
from pandas import read_csv, concat, DataFrame, to_numeric, Series, read_sql
from sklearn.metrics import accuracy_score
from sklearn.neighbors import LocalOutlierFactor

from router.settings import AGGREGATED_DATAFRAME_PATH, CLICKHOUSE_CONFIG, SOURCE_DATA_TABLE_NAME

NON_USED_COLUMNS = ["flow_id", "timestamp", "src_ip", "src_port", "dst_ip", "dst_port", "protocol", "label"]
NON_USED_COLUMNS = OrderedDict((key, key) for key in NON_USED_COLUMNS)


def get_db_engine():
    username = CLICKHOUSE_CONFIG["user"]
    password = CLICKHOUSE_CONFIG["password"]
    host = CLICKHOUSE_CONFIG["host"]
    db_name = CLICKHOUSE_CONFIG["db_name"]
    uri = f'clickhouse://{username}:{password}@{host}/{db_name}'
    return create_engine(uri)


engine = get_db_engine()


def score_model(estimator, X, y=None, test_data=None, test_labels=None):
    if test_data is None or test_labels is None:
        raise ValueError("None value for test_data or test_labels")
    res = estimator.predict(test_data)
    return accuracy_score(y_true=test_labels, y_pred=res)


def rename_columns(df):
    cols = df.columns
    cols = cols.map(lambda x: x.lower().strip().replace(' ', '_'))
    df.columns = cols
    return df


def convert_columns_type(df, miss_columns=None):
    if miss_columns is None:
        miss_columns = {}
    for col in df.columns:
        if col in miss_columns:
            continue
        df[col] = to_numeric(df[col], errors='coerce')
    return df


def remove_nans(df):
    df.replace("Infinity", 1, inplace=True)
    df.replace("NaN", 0, inplace=True)
    df.dropna(inplace=True)
    return df


def divide_dataframe(df):
    model_columns = {column for column in df.columns if column not in NON_USED_COLUMNS}
    df_for_model = df.loc[:, model_columns]
    info_df = df.loc[:, {key for key in NON_USED_COLUMNS}]
    return df_for_model, info_df


classifier = LocalOutlierFactor(n_jobs=-1)
aggregated_df = None

# if exists(AGGREGATED_DATAFRAME_PATH):
#     aggregated_df = read_pickle(AGGREGATED_DATAFRAME_PATH)
#     aggregated_df.to_sql("traffic", engine, if_exists="append", index=False)
    # df = convert_columns_type(aggregated_df, miss_columns=NON_USED_COLUMNS)
    # df = remove_nans(df)
    # df, _ = divide_dataframe(df)
    # classifier.fit_predict(df)


def update_aggregated_dataframe(current_dataframe):
    global aggregated_df
    if aggregated_df is None:
        aggregated_df = current_dataframe
    else:
        aggregated_df = concat((aggregated_df, current_dataframe))
    aggregated_df.to_pickle(AGGREGATED_DATAFRAME_PATH)
    return aggregated_df


# def fit_model(csv_path):
#     df = read_csv(csv_path)
#     df = rename_columns(df)
#     update_aggregated_dataframe(df)
#     df = convert_columns_type(df, miss_columns=NON_USED_COLUMNS)
#     df = remove_nans(df)
#     df, _ = divide_dataframe(df)
#     classifier.fit(df)


ATTACK_LABEL = "ATTACK"
BENIGN_LABEL = "BENIGN"


# def predict(csv_path):
#     df = read_csv(csv_path)
#     df = rename_columns(df)
#     df = convert_columns_type(df, miss_columns=NON_USED_COLUMNS)
#     df = remove_nans(df)
#     model_df, info_df = divide_dataframe(df)
#     labels = Series(classifier.predict(model_df))
#     for col in ["flow_id", "label"]:
#         del info_df[col]
#     info_df["predicted_label"] = labels.apply(lambda x: ATTACK_LABEL if x == -1 else BENIGN_LEVEL)
#     return info_df


def fit_predict(csv_path):
    source_df = read_csv(csv_path)
    source_df = rename_columns(source_df)
    df = update_aggregated_dataframe(source_df)
    df = convert_columns_type(df, miss_columns=NON_USED_COLUMNS)
    df = remove_nans(df)
    model_df, info_df = divide_dataframe(df)
    labels = Series(classifier.fit_predict(model_df))
    info_df["predicted_label"] = labels.apply(lambda x: ATTACK_LABEL if x == -1 else BENIGN_LABEL)
    info_ids = info_df["flow_id"] + info_df["timestamp"]
    source_ids = source_df["flow_id"] + source_df["timestamp"]
    info_df = info_df[info_ids.isin(source_ids.tolist())]
    for col in ["flow_id", "label"]:
        del info_df[col]
    return info_df


def add_new_data_from_csv(csv_path):
    source_df = read_csv(csv_path)
    source_df = rename_columns(source_df)
    Thread(target=source_df.to_sql, args=(SOURCE_DATA_TABLE_NAME, engine), kwargs={"if_exists": "append", "index": False}).start()


def get_outliers():
    df = read_sql(SOURCE_DATA_TABLE_NAME, engine)
    df = convert_columns_type(df, miss_columns=NON_USED_COLUMNS)
    df = remove_nans(df)
    model_df, info_df = divide_dataframe(df)
    model_df = model_df.to_numpy()
    model_df = preprocessing.robust_scale(model_df)
    contamination = 0.001
    classifiers = [
        IForest(contamination=contamination, n_jobs=-1, behaviour="new"),
        LOF(contamination=contamination, n_jobs=-1, algorithm="auto"),
        PCA(contamination=contamination),
        MCD(contamination=contamination)
    ]
    aggregator = SimpleDetectorAggregator(classifiers, contamination=contamination)
    aggregator.fit(model_df)
    labels = Series(aggregator.labels_)
    info_df = info_df[labels == 1]
    for col in ["flow_id", "label"]:
        del info_df[col]
    info_df.to_csv("/tmp/outliers.csv")
    return info_df


def get_chunks(df: DataFrame, chunk_size: int):
    for i in range(math.ceil(len(df) / chunk_size)):
        yield df.iloc[i*chunk_size: i*chunk_size + chunk_size, :]


def aggregate_csv(*filenames):
    common_df = concat(divide_dataframe(read_csv(csv_path)) for csv_path in filenames)
    common_df.to_csv("aggregated.csv", index=False)
