# Generated by Django 2.1.4 on 2019-02-02 18:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20190120_1443'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImplementationsGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('description', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ImplementationsGroupItems',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField()),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.ImplementationsGroup')),
                ('implementation', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.ScriptImplementation')),
            ],
        ),
        migrations.AlterField(
            model_name='script',
            name='name',
            field=models.CharField(max_length=200, unique=True),
        ),
        migrations.AddField(
            model_name='implementationsgroup',
            name='implementations',
            field=models.ManyToManyField(through='core.ImplementationsGroupItems', to='core.ScriptImplementation'),
        ),
    ]
