from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import JSONField
from django.db.models import Model, CharField, IntegerField, TextField, ForeignKey, BooleanField, CASCADE, SET_NULL, \
    ManyToManyField, PROTECT, DateTimeField


class Settings(Model):
    name = CharField(max_length=300, unique=True)
    value = CharField(max_length=200)


class User(AbstractUser):
    phone = CharField(max_length=200, default="")


class Session(Model):
    session_id = CharField(max_length=255, primary_key=True)
    payload = JSONField(default=dict)


class SSHConnection(Model):
    host_name = CharField(max_length=200)
    port = IntegerField(default=22)
    username = CharField(max_length=100)
    password = CharField(max_length=255)
    public_ssh_key = CharField(max_length=255, default="")


class ScriptType(Model):
    name = CharField(max_length=200)
    description = TextField()


class Script(Model):
    name = CharField(max_length=200, unique=True)
    description_text = CharField(max_length=200)
    # command_to_execute = DeferredForeignKey("CommandToExecute") TODO: WHY is it here?
    script_type = ForeignKey('ScriptType', null=True, on_delete=SET_NULL)


class ScriptUser(Model):
    user = ForeignKey('User', on_delete=CASCADE)
    script = ForeignKey('Script', on_delete=CASCADE)
    is_allowed = BooleanField(default=True)


class Router(Model):
    name = CharField(max_length=200, unique=True)
    ssh_connection = ForeignKey("SSHConnection", null=True, related_name="routers", on_delete=SET_NULL)


class ScriptImplementation(Model):
    router = ForeignKey(Router, on_delete=CASCADE)
    script = ForeignKey(Script, on_delete=CASCADE)

    class Meta:
        unique_together = (["router", "script"])


class CommandToExecute(Model):
    name = CharField(max_length=100)  # TODO: what is it for?
    value = CharField(max_length=255)
    order = IntegerField()
    implementation = ForeignKey(ScriptImplementation, on_delete=CASCADE)


class ImplementationsGroupItems(Model):
    order = IntegerField()
    group = ForeignKey("ImplementationsGroup", on_delete=CASCADE)
    implementation = ForeignKey("ScriptImplementation", on_delete=PROTECT)


class ImplementationsGroup(Model):
    name = CharField(max_length=100, unique=True)
    description = CharField(max_length=255)
    implementations = ManyToManyField(ScriptImplementation, through=ImplementationsGroupItems)


class TrafficAnalysisResult(Model):
    src_ip = CharField(max_length=20)
    src_port = IntegerField()
    dst_ip = CharField(max_length=20)
    dst_port = IntegerField()
    timestamp = DateTimeField()
    protocol = IntegerField()
    predicted_label = CharField(max_length=10)
