from django.core.management.base import BaseCommand
from os import path

from controller.services.load_from_config import load_data_from_file


class Command(BaseCommand):
    help = 'Loads data from given config'

    def add_arguments(self, parser):
        parser.add_argument('config_path')

    def handle(self, *args, **options):
        filepath = path.abspath(options["config_path"])
        self.stdout.write(f"Loading configuration from file {filepath}")
        load_data_from_file(filepath)
