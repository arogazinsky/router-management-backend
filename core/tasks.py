from tempfile import NamedTemporaryFile

from celery import shared_task
from core.ml import get_outliers
from django.core.cache import cache
import pandas


@shared_task
def get_outliers_task():
    buffer = NamedTemporaryFile(prefix="outliers", suffix=".gzip")
    df: pandas.DataFrame = get_outliers()
    df.to_pickle(buffer.name, compression="gzip")
    buffer.seek(0)
    cache.set("outliers.gzip", buffer.read(), None)
