from django.contrib.auth.password_validation import validate_password
from django.db.transaction import atomic
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.relations import PrimaryKeyRelatedField

from core import models
from core.models import ScriptImplementation, Settings, ImplementationsGroup, ImplementationsGroupItems, \
    TrafficAnalysisResult


class SSHConnectionSerializer(serializers.ModelSerializer):
    password = serializers.CharField(allow_blank=True)

    class Meta:
        model = models.SSHConnection
        fields = ["host_name", "port", "username", "password"]


class RouterSerializer(serializers.ModelSerializer):
    ssh_connection = SSHConnectionSerializer()

    class Meta:
        model = models.Router
        fields = ["id", "name", "ssh_connection"]
        read_only_fields = ["id"]

    @atomic
    def create(self, validated_data):
        ssh_connection = validated_data.pop("ssh_connection")
        ssh_connection = models.SSHConnection.objects.create(**ssh_connection)
        validated_data["ssh_connection"] = ssh_connection
        return models.Router.objects.create(**validated_data)

    @atomic
    def update(self, instance, validated_data):
        ssh_connection = instance.ssh_connection
        SSHConnectionSerializer().update(ssh_connection, validated_data.pop("ssh_connection"))
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        return {
            "ssh_connection": SSHConnectionSerializer(instance.ssh_connection).data,
            **super().to_representation(instance)
        }


class ScriptImplementationSerializer(serializers.ModelSerializer):
    commands = serializers.ListField(child=serializers.CharField(), write_only=True)
    router = serializers.SlugRelatedField(queryset=models.Router.objects, slug_field="name")

    class Meta:
        model = ScriptImplementation
        fields = ["id", "router", "commands"]
        read_only_fields = ["id"]

    def validate(self, attrs):
        router = attrs["router"]
        script_id = self.context["script_id"]
        implementation_qs = ScriptImplementation.objects.filter(router=router, script_id=script_id)
        if self.instance:
            implementation_qs = implementation_qs.exclude(pk=self.instance.pk)
        if implementation_qs.exists():
            raise ValidationError({"router": "Each script may have one and only one implementation for every router"})
        return attrs

    @staticmethod
    def create_commands(commands, implementation):
        for idx, command_text in enumerate(commands):
            models.CommandToExecute.objects.create(
                value=command_text,
                order=idx,
                implementation=implementation
            )

    @staticmethod
    def get_commands(implementation: ScriptImplementation):
        qs = implementation.commandtoexecute_set.order_by("order").all()
        return qs.values_list("value", flat=True)

    @atomic
    def create(self, validated_data):
        script_id = self.context["script_id"]
        implementation = ScriptImplementation.objects.create(script_id=script_id, router=validated_data["router"])
        self.create_commands(validated_data["commands"], implementation)
        return implementation

    @atomic
    def update(self, instance: ScriptImplementation, validated_data):
        instance.commandtoexecute_set.all().delete()
        self.create_commands(validated_data["commands"], instance)
        instance.router = validated_data["router"]
        instance.save()
        return instance

    def to_representation(self, instance):
        return {
            "commands": self.get_commands(instance),
            **super().to_representation(instance)
        }


class ScriptSerializer(serializers.ModelSerializer):
    allowed_for_users = serializers.SlugRelatedField(queryset=models.User.objects, slug_field="username", write_only=True, many=True)

    class Meta:
        model = models.Script
        fields = ["id", "name", "description_text", "allowed_for_users"]
        read_only_fields = ["id"]

    def add_allowed_users(self, script, users):
        script_users = [models.ScriptUser(user=user, script=script) for user in users]
        models.ScriptUser.objects.bulk_create(script_users)

    @atomic
    def create(self, validated_data):
        users = validated_data.pop("allowed_for_users")
        script = models.Script.objects.create(**validated_data)
        self.add_allowed_users(script, users)
        return script

    @atomic
    def update(self, instance: models.Script, validated_data):
        users = validated_data.pop("allowed_for_users")
        instance.scriptuser_set.all().delete()
        self.add_allowed_users(instance, users)
        super().update(instance, validated_data)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        allowed_for_users = models.ScriptUser.objects.filter(script=instance).values_list("user__username", flat=True)
        data.update(
            allowed_for_users=allowed_for_users,
        )
        return data


class TelegramUserSerializer(serializers.ModelSerializer):
    is_staff = serializers.BooleanField(default=False)

    class Meta:
        model = models.User
        fields = ["id", "username", "is_staff"]
        read_only_fields = ["id"]


class PasswordResetSerializer(serializers.Serializer):
    new_password = serializers.CharField()
    password_confirm = serializers.CharField()

    def validate(self, attrs):
        if attrs["new_password"] != attrs["password_confirm"]:
            raise ValidationError({"new_password": "New password and confirm values are different"})
        validate_password(attrs["new_password"])
        return attrs

    def create(self, validated_data):
        user = self.context.get("user")
        user.set_password(validated_data["new_password"])
        return user


class SettingsSerializer(serializers.Serializer):
    TelegramBotToken = serializers.CharField()
    AuthToken = serializers.CharField()
    CommonChannelId = serializers.CharField()
    BotState = serializers.CharField()

    def create(self, validated_data):
        for key, value in validated_data.items():
            instance = Settings.objects.get_or_create(name=key)[0]
            instance.value = value
            instance.save()
        return validated_data

    @staticmethod
    def get_json():
        get_setting = Settings.objects.get
        fields = ["TelegramBotToken", "AuthToken", "CommonChannelId", "BotState"]
        result = {}
        for name in fields:
            try:
                result[name] = get_setting(name=name).value
            except Settings.DoesNotExist:
                result[name] = None
        return result


class ImplementationsGroupSerializer(serializers.ModelSerializer):
    implementations = PrimaryKeyRelatedField(queryset=ScriptImplementation.objects.all(), many=True)

    class Meta:
        model = ImplementationsGroup
        fields = "__all__"

    @staticmethod
    def create_items(instance, implementations):
        items = [ImplementationsGroupItems(implementation=item, group=instance, order=idx)
                 for idx, item in enumerate(implementations)]
        ImplementationsGroupItems.objects.bulk_create(items)

    def create(self, validated_data):
        implementations = validated_data.pop("implementations")
        instance = ImplementationsGroup.objects.create(**validated_data)
        ImplementationsGroupSerializer.create_items(instance, implementations)
        return instance

    def update(self, instance: ImplementationsGroup, validated_data):
        instance.name = validated_data["name"]
        instance.description = validated_data["description"]
        instance.save()
        instance.implementationsgroupitems_set.all().delete()
        implementations = validated_data.pop("implementations")
        ImplementationsGroupSerializer.create_items(instance, implementations)
        return instance

    def to_representation(self, instance: ImplementationsGroup):
        return {
            "id": instance.id,
            "name": instance.name,
            "description": instance.description,
            "implementations": [
                item.implementation_id for item in instance.implementationsgroupitems_set.all().order_by("order")
            ]
        }


class TrafficAnalysisResultSerializer(serializers.ModelSerializer):
    timestamp = serializers.DateTimeField(input_formats=["%d/%m/%Y %I:%M:%S %p"])

    class Meta:
        model = TrafficAnalysisResult
        fields = "__all__"
